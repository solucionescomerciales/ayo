const slider = document.querySelector('.slider');
let slideIndex = 0;

function showSlides() {
  slideIndex++;
  if (slideIndex >= slider.children.length) {
    slideIndex = 0;
  }

  const translateValue = -slideIndex * 100;
  slider.style.transform = `translateX(${translateValue}%)`;

  setTimeout(showSlides, 3000); // Cambia el slide cada 3 segundos (ajusta según necesites)
}

showSlides();